COGENT_FILES	 = Almondbread.cogent

AC_FILES	 = main.ac
AH_FILES	 = abstract/Grid.ah
# EXT_TYPES	 = types.cfg
# ENTRY_FUNCS	 = entrypoints.cfg

STDGUM		:= $(shell cogent --stdgum-dir)
CFLAGS		+= -I${STDGUM} -I${STDGUM}gum/anti -I. -O2
COGENTFLAGS	 = --cpp=cpp \
		   --cpp-args="\$$CPPIN -o \$$CPPOUT -P $(CFLAGS)" \
		   --fno-fncall-as-macro \
		   --ffunc-purity-attr \

AC_PP		:= $(AC_FILES:.ac=_pp.ac)
AC_PPINFER	:= $(AC_FILES:.ac=_pp_inferred.c)
AC_OBJS		:= $(AC_PPINFER:.c=.o)

all:	main

main:	${AC_OBJS} -lm
	$(CC) $(CFLAGS) -o $@ $^

${AC_PPINFER}: ${AC_FILES} ${AH_FILES} ${COGENT_FILES} ${EXT_TYPES} ${ENTRY_FUNCS}
	cogent \
		${COGENTFLAGS} \
		-o generated -g \
		$(if ${AH_FILES},--infer-c-types="${AH_FILES}") \
		$(if ${AC_FILES},--infer-c-funcs="${AC_FILES}") \
		$(if ${EXT_TYPES},--ext-types=${EXT_TYPES}) \
		$(if ${ENTRY_FUNCS},--entry-funcs=${ENTRY_FUNCS}) \
		${COGENT_FILES}

.PHONY: clean
clean:
	-rm -f ${AC_PP} ${AC_PPINFER} ${AC_OBJS}
	-rm -f generated.c generated.h abstract/*.h
	-rm -f main

